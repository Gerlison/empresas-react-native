
![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Aplicativo React Native para busca de empresas. O aplicativo permite buscar e visualizar empresas disponíveis na base de dados.

---

### Instruções de instalação

Clone o projeto
```shell
$ git clone https://Gerlison@bitbucket.org/Gerlison/empresas-react-native.git
```
  
Navegue até o diretório e instale as dependências
```shell
$ cd empresas-react-native/app/

$ npm install
```

Tudo pronto, agora apenas execute o project
```shell
$ react-native run-android
```

> Para executar o projeto é necessário um emulador ou device conectado.

### Dependências
  
Esse projeto foi criado utilizando meu custom template [rn-boilerplate](https://github.com/Gerlison/rn_boilerplate), o qual já vem com algumas cofigurações preparadas, estrutura de pastas, etc. Suas configuações e especificações estão detalhadas no [README](https://github.com/Gerlison/rn_boilerplate/blob/master/README.md) do mesmo.

A baixo estão listadas as dependências utilizadas nesse projeto:

* [React-Navigation](https://reactnavigation.org/docs/en/getting-started.html) - solução para navegação entre páginas.
* [Redux](https://redux.js.org/) - para gerênciar o estado de forma centralizada. 
* [Redux-Persist](https://redux.js.org/) - persistência para os dados do redux.
* [Reselect](https://github.com/reduxjs/reselect) - para seletores avançados, permitindo uma maior performance de atualizações do redux.
* [Redux-Saga](https://github.com/redux-saga/redux-saga) - gerenciamento de side-effects, usado para tasks asyncronas.
* [Axios](https://github.com/axios/axios) - HTTP client, para execução de chamadas a API.
* [Lodash](https://github.com/lodash/lodash) - prover funções para controle de objetos e arrays.
* [Styled-Components](https://www.styled-components.com/) - ajuda com a estilização de componentes.
* [Feather1s](https://github.com/sebinq/react-native-feather1s)- utilizado para iconografia. Usa react-native-vector-icons como peer dependency.
* [Flow](https://github.com/facebook/flow) - tipagem estática para javascript.
* [Formik](https://github.com/jaredpalmer/formik) - generenciamento de formulários
* [yup](https://github.com/jquense/yup) - validação dos formulários
* [Babel-module-resolver](https://github.com/tleunen/babel-plugin-module-resolver) - para facilitar o import de arquivos
* [react-native-extra-dimensions-android](https://github.com/Sunhat/react-native-extra-dimensions-android) - para capturar as dimensões corretas dos devices que não possuem botões de navegação


### Release

Para testes em produção enviei uma versão **incompleta** do projeto, está disponível apenas para android e pode ser adquirida [aqui](https://drive.google.com/open?id=1adjPNUNVLaSaoXyFtUGqrGaUT2JbXUGS).


### Desenvolvimento

Ambiente no qual o projeto foi desenvolvido:
```
NAME="KDE neon"
VERSION="5.17"
ID=neon
ID_LIKE="ubuntu debian"
PRETTY_NAME="KDE neon User Edition 5.17"
VARIANT="User Edition"
VERSION_ID="18.04"
HOME_URL="http://neon.kde.org/"
SUPPORT_URL="http://neon.kde.org/"
BUG_REPORT_URL="http://bugs.kde.org/"
LOGO=start-here-kde-neon
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=bionic
UBUNTU_CODENAME=bionic
```

---
IDE
```
Visual Studio Code
```

---
Dispositivos testados

* Xiaomi Redmi Note 7 - android 9
* Nexux 5 - android 7.0

> Nenhum teste foi realizado no iOS
