//@flow
import * as React from 'react';

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import storeFactory from '~/store';
const {store, persistor} = storeFactory;

import Router from './src/navigation/layout';
import ErrorBoundary from '~shared/components/ErrorBoundary';
import ThemeProvider from '~shared/components/ThemeProvider';

import Splash from '~shared/components/Splash';

export default function(): React.Element<any> {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider>
          <ErrorBoundary>
            <Router />
          </ErrorBoundary>
        </ThemeProvider>
      </PersistGate>
    </Provider>
  );
}
