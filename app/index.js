/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';

const APP_NAME = 'app';

AppRegistry.registerComponent(APP_NAME, () => App);
