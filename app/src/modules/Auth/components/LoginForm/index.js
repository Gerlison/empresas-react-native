import React, {useRef} from 'react';
import {View} from 'react-native';
import {Formik} from 'formik';
import styled from 'styled-components/native';
import {spacing} from '~shared/styles/metrics';
import {Text} from '~shared/styles/general';

import {LineInput} from '~shared/components/Input';
import Button from '~shared/components/Button';
import model from './model';

import {useDispatch} from 'react-redux';
import {loginUserAsync} from '../../ducks/User/actions';

const INITIAL_VALUES: FormFields = {email: '', password: ''};

const LoginForm = ({loadingLoginUser}) => {
  const dispatch = useDispatch();
  const {current} = useRef({current: null});

  function handleSubmit(values) {
    if (values) {
      dispatch(loginUserAsync(values));
    }
  }

  function handleInputRef(ref, label) {
    current[label] = ref;
  }

  function focusNextInput(label) {
    current[label].focus();
  }

  return (
    <Formik
      validationSchema={model}
      initialValues={INITIAL_VALUES}
      onSubmit={handleSubmit}>
      {({handleChange, values, handleSubmit: submitForm, touched, errors}) => {
        function getFormError(field) {
          return touched[field] && errors[field];
        }

        return (
          <View>
            <StyledText
              marginBottom="LARGEST"
              size="LARGEST"
              weight="BOLD"
              colors="PRIMARY">
              Login
            </StyledText>

            <StyledLineInput
              name="mail"
              color={getFormError('email') && 'DANGER'}
              onSubmitEditing={() => focusNextInput('password')}
              onChangeText={handleChange('email')}
              value={values.email}
              blurOnSubmit={false}
              placeholder="E-mail"
              returnKeyType="next"
              autoCapitalize="none"
            />
            <StyledText colors="DANGER" size="SMALL" marginBottom="SMALL">
              {getFormError('email')}
            </StyledText>

            <StyledLineInput
              name="lock"
              color={getFormError('password') && 'DANGER'}
              getInputRef={ref => handleInputRef(ref, 'password')}
              onChangeText={handleChange('password')}
              onSubmitEditing={submitForm}
              value={values.password}
              placeholder="Senha"
              returnKeyType="go"
              secureTextEntry
            />
            <StyledText colors="DANGER" size="SMALL" marginBottom="LARGEST">
              {getFormError('password')}
            </StyledText>

            <Button
              loading={loadingLoginUser}
              containerStyle={{marginBottom: spacing.LARGE * 2}}
              onPress={submitForm}
              label="entrar"
            />
          </View>
        );
      }}
    </Formik>
  );
};

const StyledLineInput = styled(LineInput).attrs(({color, name}) => ({
  leftIconStyle: {
    name,
    size: 18,
    color,
  },
}))``;

const StyledText = styled(Text)`
  margin-bottom: ${({marginBottom}) => spacing[marginBottom]};
`;
export default LoginForm;
