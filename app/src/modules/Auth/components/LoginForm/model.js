import * as yup from 'yup';

const schema = yup.object().shape({
  email: yup
    .string()
    .label('E-mail')
    .email('E-mail inválido')
    .required('O campo de e-mail é obrigatório'),
  password: yup
    .string()
    .label('Senha')
    .required('O campo de senha é obrigatório'),
});

if (__DEV__) {
  schema.isValid({
    email: 'jimmy@email.com',
    password: 'pass',
  });
}
// .then(console.warn);

export default schema;
