import TYPES from './types';
const {LOGIN_USER_REQUEST, LOGOUT_USER} = TYPES;

function loginUserAsync(payload) {
  return {
    type: LOGIN_USER_REQUEST,
    payload,
  };
}

function logoutUser() {
  return {
    type: LOGOUT_USER,
  };
}

export {loginUserAsync, logoutUser};
