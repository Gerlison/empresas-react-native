import TYPES from './types';
const {LOGIN_USER_SUCCESS, LOGOUT_USER} = TYPES;

const INITIAL_STATE: State = {
  data: null,
  isAuthenticated: false,
  authHeaders: {
    'Content-Type': 'application/json',
    'access-token': null,
    client: null,
    uid: null,
  },
};

export default function(state = INITIAL_STATE, action) {
  const {type, payload} = action;

  switch (type) {
    case LOGIN_USER_SUCCESS:
      return Object.assign(state, {
        data: payload.data,
        authHeaders: payload.responseHeaders,
        isAuthenticated: true,
      });

    case LOGOUT_USER:
      return Object.assign(state, {
        data: null,
        isAuthenticated: false,
        authHeaders: {
          'Content-Type': 'application/json',
          'access-token': null,
          client: null,
          uid: null,
        },
      });

    default:
      return state;
  }
}
