import {takeLatest, put, fork, call} from 'redux-saga/effects';
import Api from '../api';

import type {Action} from '~shared/helpers/Types';
import TYPES from './User/types';
const {LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE} = TYPES;

function* loginUser(action: Action) {
  try {
    const user = yield call(Api.login, action.payload);
    yield put({type: LOGIN_USER_SUCCESS, payload: user});
  } catch (err) {
    yield put({type: LOGIN_USER_FAILURE});
  }
}

function* watchLoginUser() {
  yield takeLatest(LOGIN_USER_REQUEST, loginUser);
}

export default function* root() {
  yield fork(watchLoginUser);
}
