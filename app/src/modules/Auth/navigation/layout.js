import {createStackNavigator} from 'react-navigation-stack';

import Login from '../screens/LoginScreen';

const AuthStack = createStackNavigator(
  {
    Login,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);

export default AuthStack;
