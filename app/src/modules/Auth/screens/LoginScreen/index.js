import React, {useEffect} from 'react';
import {Platform} from 'react-native';
import styled from 'styled-components/native';
import {useSelector} from 'react-redux';

import {Container} from '~shared/styles/general';
import {spacing} from '~shared/styles/metrics';
import {WINDOW_HEIGHT} from '~shared/helpers/Dimensions';

import LoginForm from '../../components/LoginForm';

import {getUser} from '~shared/helpers/Selectors';

const Login = ({navigation, loginUserAsync}) => {
  const user = useSelector(getUser);
  const {loadingLoginUser} = useSelector(({loading}) => loading);

  const {isAuthenticated} = user;
  useEffect(() => {
    if (isAuthenticated) {
      navigation.navigate('Enterprise');
    }
  }, [navigation, isAuthenticated]);

  return (
    <StyledKeyboardAvoidingView>
      <Container>
        <StyledScrollView>
          <StyledLogo />
          <LoginForm loadingLoginUser={loadingLoginUser} user={user} />
        </StyledScrollView>
      </Container>
    </StyledKeyboardAvoidingView>
  );
};

const StyledLogo = styled.Image.attrs(({theme}) => ({
  source:
    theme.currentTheme === 'light'
      ? require('~images/logo_ioasys.png')
      : require('~images/logo_ioasys_dark.png'),
  resizeMode: 'contain',
}))`
  width: 100%;
  height: 100px;

  margin-top: ${spacing.LARGEST}px;
  margin-bottom: ${spacing.LARGEST * 3}px;
`;

const StyledScrollView = styled.ScrollView.attrs(() => ({
  contentContainerStyle: {
    minHeight: Platform.OS === 'android' ? WINDOW_HEIGHT : '100%',
    padding: spacing.MEDIUM * 2,
  },
}))``;

const StyledKeyboardAvoidingView = styled.KeyboardAvoidingView`
  flex: 1;
`;

export default Login;
