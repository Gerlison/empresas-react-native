// import axios from 'axios';

// const Api = axios.create({
//   baseURL: 'https://empresas.ioasys.com.br/api/v1',
// });

function getEnterprises(headers) {
  // return Api.get('/enterprises');
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        responseHeaders: {
          'Content-Type': 'application-json',
          'access-token': '548-fnJaeQd6WL-De3WzHg',
          client: 'uRxPHIQJOG2G4cmj9ABQkA',
          uid: 'testeapple@ioasys.com.br',
        },
        data: [
          {
            id: 1,
            title: 'Ioasys',
            description:
              'Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus.',
            type: {
              id: 1,
              label: 'Tecnology',
            },
          },
          {
            id: 1,
            title: 'IntellGest',
            description:
              'Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus.',
            type: {
              id: 2,
              label: 'Tecnology',
            },
          },
          {
            id: 1,
            title: 'Gerlison',
            description:
              'Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus.',
            type: {
              id: 3,
              label: 'Tecnology',
            },
          },
          {
            id: 1,
            title: 'Google',
            description:
              'Proin eget tortor risus. Nulla quis lorem ut libero malesuada feugiat. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Proin eget tortor risus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Proin eget tortor risus.',
            type: {
              id: 4,
              label: 'Tecnology',
            },
          },
        ],
      });
    }, 1000);
  });
}

export default {getEnterprises};
