import React from 'react';
import {Text} from '~shared/styles/general';

import styled from 'styled-components/native';
import {spacing} from '~shared/styles/metrics';

const CheckBox = ({selected, label}) => (
  <StyledContainer checked={selected}>
    <Text
      lightColor={selected ? 'WHITE' : 'PRIMARY'}
      darkColor={selected ? 'DARKEST' : 'PRIMARY'}>
      {label}
    </Text>
  </StyledContainer>
);

const StyledContainer = styled.View`
  height: 40px;
  justify-content: center;
  margin: ${spacing.SMALLEST}px;
  padding-horizontal: ${spacing.MEDIUM}px;

  border-width: 1px;
  border-color: ${({theme}) => theme.PRIMARY};

  background-color: ${({checked, theme}) =>
    checked ? theme.PRIMARY : 'transparent'};
`;

export default CheckBox;
