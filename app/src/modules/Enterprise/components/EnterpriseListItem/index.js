import * as React from 'react';
import {View} from 'react-native';
import {Text, Icon} from '~shared/styles/general';

import styled from 'styled-components/native';
import {spacing} from '~shared/styles/metrics';

const EnterpriseListItem = ({description, type, title}) => (
  <StyledContainer>
    <StyledHeader>
      <View>
        <Text colors="TERTIARY" size="LARGE">
          {title}
        </Text>
        <Text size="SMALL" colors="REGULAR">
          {type}
        </Text>
      </View>
      <Icon name="chevrons-right" size="LARGE" colors="REGULAR" />
    </StyledHeader>
    <Text numberOfLines={3}>{description}</Text>
  </StyledContainer>
);

const StyledContainer = styled.View`
  margin-left: ${spacing.LARGE}px;
  margin-bottom: ${spacing.LARGE};
  padding-right: ${spacing.LARGE}px;
  padding-bottom: ${spacing.LARGE}px;

  border-bottom-width: 0.5px;
  border-color: ${({theme}) => theme.REGULAR};
`;

const StyledHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${spacing.SMALL}px;
`;

export default EnterpriseListItem;
