import * as React from 'react';
import {FlatList, View} from 'react-native';
import CheckBox from '../CheckBox';
import {spacing} from '~shared/styles/metrics';

const TypeFilter = ({types}) => (
  <View>
    <FlatList
      horizontal
      data={types}
      keyExtractor={item => item.id.toString()}
      contentContainerStyle={{paddingLeft: spacing.LARGE}}
      showsHorizontalScrollIndicator={false}
      renderItem={({item: {selected, label}, index}) => (
        <CheckBox selected={selected} label={label} />
      )}
    />
  </View>
);

export default TypeFilter;
