import TYPES from './types';
const {GET_ENTERPRISES_REQUEST} = TYPES;

function getEnterprisesAsync() {
  return {
    type: GET_ENTERPRISES_REQUEST,
  };
}

export {getEnterprisesAsync};
