import TYPES from './types';
const {GET_ENTERPRISES_SUCCESS} = TYPES;

const INITIAL_STATE = {
  data: null,
};

export default function(state = INITIAL_STATE, action) {
  const {type, payload} = action;

  switch (type) {
    case GET_ENTERPRISES_SUCCESS:
      return Object.assign(state, {
        data: payload.data,
      });

    default:
      return state;
  }
}
