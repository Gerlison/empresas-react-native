import {fork, select, takeEvery, put, call} from 'redux-saga/effects';
import Api from '../api';
import TYPES from './Enterprise/types';
const {
  GET_ENTERPRISES_SUCCESS,
  GET_ENTERPRISES_REQUEST,
  GET_ENTERPRISES_FAILURE,
} = TYPES;

function* getEnterprises() {
  try {
    const headers = yield select(({user: {authHeaders}}) => authHeaders);
    const response = yield call(Api.getEnterprises, headers);
    yield put({type: GET_ENTERPRISES_SUCCESS, payload: response});
  } catch (err) {
    yield put({type: GET_ENTERPRISES_FAILURE});
  }
}

function* watchGetEnterprises() {
  yield takeEvery(GET_ENTERPRISES_REQUEST, getEnterprises);
}

export default function* root() {
  yield fork(watchGetEnterprises);
}
