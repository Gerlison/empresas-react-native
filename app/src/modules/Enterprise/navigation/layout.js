import {createStackNavigator} from 'react-navigation-stack';

import List from '../screens/ListScreen';
import View from '../screens/ViewScreen';
import Filter from '../screens/FilterScreen';

const AuthStack = createStackNavigator(
  {
    List,
    View,
    Filter,
  },
  {
    initialRouteName: 'List',
    headerMode: 'none',
  },
);

export default AuthStack;
