import React from 'react';
import {TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';

import {Container, Icon} from '~shared/styles/general';
import {spacing} from '~shared/styles/metrics';

import Input from '~shared/components/Input';
import EnterpriseListItem from '../../components/EnterpriseListItem';
import TypeFilter from '../../components/TypeFilter';

const FilterScreen = ({navigation}) => (
  <Container>
    <StyledHeader>
      <TouchableOpacity onPress={() => navigation.navigate('List')}>
        <StyledIcon name="arrow-left" size="LARGER" thin={false} />
      </TouchableOpacity>
      <StyledInput autoFocus placeholder="Pesquisar" />
    </StyledHeader>
    <TypeFilter
      types={[
        {id: 1, label: 'Tecnology', selected: true},
        {id: 2, label: 'Eco'},
      ]}
    />
    <StyledFlatList
      data={[]}
      keyExtractor={(_, index) => index.toString()}
      renderItem={({item}) => (
        <TouchableOpacity onPress={() => navigation.navigate('View', 1)}>
          <EnterpriseListItem title="Title" type="type" description="" id={1} />
        </TouchableOpacity>
      )}
    />
  </Container>
);

const StyledHeader = styled.View`
  flex-direction: row;
  align-items: center;
  padding: ${spacing.LARGE}px;
`;

const StyledInput = styled(Input).attrs(() => ({
  containerStyle: {
    flex: 1,
  },
}))``;

const StyledIcon = styled(Icon)`
  margin-right: ${spacing.MEDIUM};
`;

const StyledFlatList = styled.FlatList.attrs(() => ({
  contentContainerStyle: {
    marginTop: spacing.LARGE * 2,
  },
}))``;

export default FilterScreen;
