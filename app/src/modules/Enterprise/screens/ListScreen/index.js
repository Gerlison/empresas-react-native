import React, {useEffect, useCallback} from 'react';
import {View, TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';

import {Container, Text, Icon, Loading} from '~shared/styles/general';
import {spacing} from '~shared/styles/metrics';

import {useSelector, useDispatch} from 'react-redux';
import {logoutUser} from '~/modules/Auth/ducks/User/actions';
import {getEnterprisesAsync} from '../../ducks/Enterprise/actions';
import {toggleTheme} from '~/store/ducks/Theme/actions';

import EnterpriseListItem from '../../components/EnterpriseListItem';

import _ from 'lodash';
import {getUser, getEnterprise} from '~shared/helpers/Selectors';

function ListScreen({navigation}) {
  const dispatch = useDispatch();
  const user = useSelector(getUser);
  const enterprise = useSelector(getEnterprise);
  const {loadingGetEnterprises} = useSelector(({loading}) => loading);

  useEffect(() => {
    dispatch(getEnterprisesAsync());
  }, [dispatch]);

  const handleToggleTheme = useCallback(() => dispatch(toggleTheme()), [
    dispatch,
  ]);

  function handleLogout() {
    dispatch(logoutUser());
    navigation.navigate('Splash');
  }

  function _getUserName() {
    return _.get(user, 'data.investor.investor_name').split(' ')[0];
  }

  function renderList() {
    return (
      <StyledFlatList
        data={enterprise.data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('View', {
                title: item.title,
                id: item.id,
              })
            }>
            <EnterpriseListItem
              title={item.title}
              type={item.type.label}
              description={item.description}
            />
          </TouchableOpacity>
        )}
      />
    );
  }

  return (
    <Container>
      <StyledHeader>
        <View>
          <StyledWelcome>
            <TouchableOpacity onPress={handleLogout}>
              <StyledIcon name="log-out" size="MEDIUM" thin={false} />
            </TouchableOpacity>
            <Text colors="REGULAR" size="SMALL">
              Welcome, <Text size="SMALL">{_getUserName()}</Text>
            </Text>
          </StyledWelcome>
          <Text weight="MEDIUM" size="LARGEST" colors="PRIMARY">
            Enterprises
          </Text>
        </View>
        <StyledView>
          <StyledTouchableOpacity onPress={handleToggleTheme}>
            <Icon
              name="toggle-right"
              size="LARGER"
              thin={false}
              colors="TERTIARY"
            />
          </StyledTouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Filter')}>
            <Icon name="filter" size="LARGER" thin={false} colors="TERTIARY" />
          </TouchableOpacity>
        </StyledView>
      </StyledHeader>
      {loadingGetEnterprises ? <StyledLoading size="large" /> : renderList()}
    </Container>
  );
}

const StyledHeader = styled.View`
  flex-direction: row;
  align-items: flex-end;
  justify-content: space-between;

  margin-bottom: ${spacing.SMALL}px;
  padding-top: ${spacing.LARGE * 2}px;
  padding-horizontal: ${spacing.LARGE}px;
`;

const StyledWelcome = styled.View`
  flex-direction: row;
`;

const StyledIcon = styled(Icon)`
  margin-right: ${spacing.SMALLEST};
`;

const StyledView = styled.View`
  flex-direction: row;
`;

const StyledTouchableOpacity = styled.TouchableOpacity`
  margin-right: ${spacing.LARGE * 2};
`;

const StyledLoading = styled(Loading)`
  margin-top: ${spacing.LARGE};
`;

const StyledFlatList = styled.FlatList.attrs(() => ({
  contentContainerStyle: {
    marginTop: spacing.LARGE * 2,
    paddingBottom: spacing.MEDIUM,
  },
}))``;

export default ListScreen;
