// @flow
import React from 'react';
import {TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';

import {Container, Text, Icon} from '~shared/styles/general';
import {spacing} from '~shared/styles/metrics';

const ViewScreen = ({navigation}) => (
  <Container>
    <StyledHeader>
      <TouchableOpacity onPress={() => navigation.goBack(null)}>
        <StyledIcon name="arrow-left" size="LARGER" thin={false} />
      </TouchableOpacity>
      <Text weight="MEDIUM" size="LARGEST" colors="PRIMARY">
        Enterprises
      </Text>
    </StyledHeader>
    <StyledInfo>
      <Text>
        <Text colors="REGULAR">Located at</Text> Santiago, Chile
      </Text>
      <Text>
        100 <Text colors="REGULAR">shares</Text>
      </Text>
      <Text>
        <Text colors="REGULAR">Share price of</Text> 10000.0
      </Text>
    </StyledInfo>
    <StyledText>
      Urbanatika is a socio-environmental company with economic impact, creator
      of the agro-urban industry. We want to involve people in the processes of
      healthy eating, recycling and reuse of organic waste and the creation of
      citizen green areas. With this we are creating smarter cities from the
      people and at the same time the forest city. Urbanatika, Agro-Urban
      Industry
    </StyledText>
  </Container>
);

const StyledIcon = styled(Icon)`
  margin-horizontal: ${spacing.MEDIUM};
`;

const StyledText = styled(Text)`
  margin: ${spacing.LARGE}px;
`;

const StyledHeader = styled.View`
  flex-direction: row;
  align-items: center;
  padding-vertical: ${spacing.LARGE * 2}px;
`;

const StyledInfo = styled.View`
  justify-content: center;

  margin-left: ${spacing.LARGE}px;
  padding: ${spacing.LARGE}px;

  border-left-width: 5px;
  border-color: ${({theme}) => theme.TERTIARY};
`;

export default ViewScreen;
