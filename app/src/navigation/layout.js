import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import Splash from '~shared/components/Splash';
import Auth from '~/modules/Auth/navigation/layout';
import Enterprise from '~/modules/Enterprise/navigation/layout';

const AppNavigator = createSwitchNavigator(
  {
    Auth,
    Enterprise,
    Splash,
  },
  {
    initialRouteName: 'Splash',
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
