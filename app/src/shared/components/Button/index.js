import * as React from 'react';
import {Text, Loading} from '~shared/styles/general';

import styled from 'styled-components/native';
import {dimensions} from '~shared/styles/metrics';

const Button = ({
  disabled,
  transparent,
  loading,
  children,
  label,
  textColor,
  color,
  onPress,
}) => {
  function getTextColor() {
    if (disabled && transparent) {
      return 'DISABLED';
    }

    return textColor || (transparent ? 'PRIMARY' : 'WHITE');
  }

  function renderChild() {
    if (loading) {
      return <Loading color={transparent ? 'PRIMARY' : 'WHITE'} />;
    }

    if (label) {
      return (
        <Text weight="MEDIUM" colors={getTextColor()}>
          {label.toUpperCase()}
        </Text>
      );
    }
    return children;
  }

  return (
    <StyledContainer
      color={color}
      transparent={transparent}
      disabled={disabled}
      onPress={onPress}
      loading={loading}>
      {renderChild()}
    </StyledContainer>
  );
};

const StyledContainer = styled.TouchableOpacity.attrs(
  ({disabled, loading, onPress}) => ({
    activeOpacity: 0.8,
    onPress: disabled || loading ? null : onPress,
  }),
)`
  flex-direction: row;
  flex-grow: ${({grow}) => (grow ? '1' : '0')};

  align-items: center;
  justify-content: center;

  height: ${dimensions.button.HEIGHT}px;
  padding: ${dimensions.button.PADDING}px;

  background-color: ${({transparent, theme, disabled, color}) =>
    transparent
      ? 'transparent'
      : theme[disabled ? 'DISABLED' : color || 'TERTIARY']};
`;

export default Button;
