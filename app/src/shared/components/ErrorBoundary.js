import React, {Component} from 'react';
import {ScrollView} from 'react-native';
import {Container, Text} from '../styles/general';
import StatusBar from './StatusBar';
import styled from 'styled-components/native';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      error: null,
      errorInfo: null,
    };
  }

  static getDerivedStateFromError(error) {
    return {hasError: true, error};
  }

  componentDidCatch(error, errorInfo) {
    // logErrorToMyService(error, errorInfo);
    this.setState({errorInfo});
  }

  render() {
    const {children} = this.props;
    const {hasError, error, errorInfo} = this.state;

    if (hasError) {
      return (
        <ScrollView>
          <StyledContainer>
            <Text weight="BOLD" size="LARGER" color="DANGER">
              Something is wrong!
            </Text>
            <Text>{error.toString()}</Text>
            <Text color="SECUNDARY_LIGHT">
              {errorInfo && JSON.stringify(errorInfo)}
            </Text>
          </StyledContainer>
        </ScrollView>
      );
    }

    return (
      <>
        <StatusBar />
        {children}
      </>
    );
  }
}

const StyledContainer = styled(Container)`
  justify-content: 'center';
  padding: 18;
`;

export default ErrorBoundary;
