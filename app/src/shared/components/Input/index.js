import React, {useEffect, useRef, useContext, useState} from 'react';
import {Keyboard, TouchableOpacity} from 'react-native';

import {ThemeContext} from 'styled-components';
import Icon from 'react-native-feather1s';

import styled from 'styled-components/native';
import {dimensions} from '~shared/styles/metrics';
import {sizing} from '~shared/styles/fonts';
import {pickThemedColor} from '~shared/helpers/Theme';

const Input = props => {
  const {getInputRef, containerStyle, leftIconStyle, rightIconStyle} = props;
  const inputRef = useRef(null);
  const theme = useContext(ThemeContext);
  const [focus, setFocus] = useState(false);

  useEffect(() => {
    const keyboardListener = Keyboard.addListener('keyboardDidHide', () => {
      inputRef.current.blur();
    });

    return function() {
      keyboardListener.remove();
    };
  }, []);

  function handleInputRef(ref: Object): void {
    inputRef.current = ref;
    if (getInputRef) {
      getInputRef(ref);
    }
  }

  return (
    <StyledContainer {...containerStyle} focus={focus}>
      {leftIconStyle && (
        <Icon
          name={leftIconStyle.name}
          size={leftIconStyle.size}
          color={theme[leftIconStyle.color || 'PRIMARY']}
        />
      )}

      <StyledTextInput
        ref={handleInputRef}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        {...props}
      />

      {rightIconStyle && (
        <TouchableOpacity onPress={rightIconStyle.onPress}>
          <StyledIcon
            name={rightIconStyle.name}
            size={rightIconStyle.size}
            color={theme[rightIconStyle.color || 'PRIMARY']}
          />
        </TouchableOpacity>
      )}
    </StyledContainer>
  );
};

export const LineInput = props => {
  const {getInputRef, containerStyle, leftIconStyle, rightIconStyle} = props;
  const inputRef = useRef(null);
  const theme = useContext(ThemeContext);
  const [focus, setFocus] = useState(false);

  useEffect(() => {
    const keyboardListener = Keyboard.addListener('keyboardDidHide', () => {
      inputRef.current.blur();
    });

    return function() {
      keyboardListener.remove();
    };
  }, []);

  function handleInputRef(ref: Object): void {
    inputRef.current = ref;
    if (getInputRef) {
      getInputRef(ref);
    }
  }

  return (
    <StyledLineContainer {...containerStyle} focus={focus}>
      {leftIconStyle && (
        <StyledIcon
          name={leftIconStyle.name}
          size={leftIconStyle.size}
          color={theme[leftIconStyle.color || 'PRIMARY']}
        />
      )}

      <StyledTextInput
        ref={handleInputRef}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        {...props}
      />

      {rightIconStyle && (
        <TouchableOpacity onPress={rightIconStyle.onPress}>
          <StyledIcon
            name={rightIconStyle.name}
            size={rightIconStyle.size}
            color={theme[rightIconStyle.color || 'PRIMARY']}
          />
        </TouchableOpacity>
      )}
    </StyledLineContainer>
  );
};

const StyledContainer = styled.View`
  height: ${dimensions.input.HEIGHT}px;

  flex-direction: row;
  align-items: center;

  padding-horizontal: ${dimensions.input.PADDING}px;

  border-width: ${({focus}) => (focus ? 1 : 0)};
  border-color: ${({theme}) => theme.FOCUS};

  background-color: ${({theme}) =>
    pickThemedColor({light: theme.LIGHT, dark: theme.SECUNDARY_DARK})};
`;

const StyledLineContainer = styled.View`
  height: ${dimensions.input.HEIGHT}px;

  flex-direction: row;
  align-items: center;

  padding-horizontal: ${dimensions.input.PADDING / 2}px;
  border-bottom-width: ${({focus}) => (focus ? 1 : 0.5)};
  border-color: ${({focus, theme}) => (focus ? theme.FOCUS : theme.PRIMARY)};
`;

const StyledTextInput = styled.TextInput.attrs(({theme}) => ({
  placeholderTextColor: pickThemedColor({light: theme.DARK, dark: theme.LIGHT}),
  selectTextOnFocus: true,
}))`
  flex: 1;
  color: ${({theme}) =>
    pickThemedColor({light: theme.DARKEST, dark: theme.LIGHT})};
  font-size: ${() => sizing.SMALL};
`;

const StyledIcon = styled(Icon)`
  margin-right: 8;
`;

export default Input;
