import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';
import styled from 'styled-components/native';
import {Container as GenericContainer} from '../../styles/general';

import {useSelector} from 'react-redux';

const Splash = ({navigation}) => {
  const {isAuthenticated} = useSelector(({user}) => user);

  useEffect(() => {
    setTimeout(() => {
      if (isAuthenticated) {
        navigation.navigate('Enterprise');
      } else {
        navigation.navigate('Login');
      }
    }, 500);
  }, [navigation, isAuthenticated]);

  return (
    <StyledContainer>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <StyledLogo />
    </StyledContainer>
  );
};

const StyledContainer = styled(GenericContainer)`
  align-items: center;
  justify-content: center;
  background-color: #fff;
`;

const StyledLogo = styled.Image.attrs(() => ({
  source: require('~images/logo_ioasys_splash.png'),
  redizeMode: 'contain',
}))`
  width: 100px;
  height: 100px;
`;

export default Splash;
