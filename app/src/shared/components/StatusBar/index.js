import * as React from 'react';
import {StatusBar as Bar} from 'react-native';
import {withTheme} from 'styled-components/native';

const StatusBar = ({theme: {currentTheme, BACKGROUND}}) => {
  function getTheme() {
    return currentTheme === 'light' ? 'dark' : 'light';
  }

  return (
    <Bar backgroundColor={BACKGROUND} barStyle={`${getTheme()}-content`} />
  );
};

export default withTheme(StatusBar);
