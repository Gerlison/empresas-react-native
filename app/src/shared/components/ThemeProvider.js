import React, {cloneElement, Children} from 'react';

import {ThemeProvider as StyledComponentsProvider} from 'styled-components';
import {useSelector} from 'react-redux';

const ThemeProvider = ({children}) => {
  const {theme, currentTheme} = useSelector(state => state.theme);

  return (
    <StyledComponentsProvider theme={{...theme, currentTheme: currentTheme}}>
      {cloneElement(Children.only(children))}
    </StyledComponentsProvider>
  );
};

export default ThemeProvider;
