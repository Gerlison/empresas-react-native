import ExtraDimensions from 'react-native-extra-dimensions-android';

const SOFT_MENU_BAR_HEIGHT = ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT');
const STATUS_BAR_HEIGHT = ExtraDimensions.get('STATUS_BAR_HEIGHT');
const WINDOW_WIDTH = ExtraDimensions.get('REAL_WINDOW_WIDTH');
const WINDOW_HEIGHT =
  ExtraDimensions.get('REAL_WINDOW_HEIGHT') - STATUS_BAR_HEIGHT;

export {WINDOW_HEIGHT, WINDOW_WIDTH, STATUS_BAR_HEIGHT, SOFT_MENU_BAR_HEIGHT};
