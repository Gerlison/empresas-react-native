import {createSelectorCreator, defaultMemoize} from 'reselect';
import _ from 'lodash';

const createDeepEqualSelector = createSelectorCreator(
  defaultMemoize,
  _.isEqual,
);

const getUser = createDeepEqualSelector(
  ({user}) => user,
  user => user,
);

const getEnterprise = createDeepEqualSelector(
  ({enterprise}) => enterprise,
  enterprise => enterprise,
);

export {getUser, getEnterprise};
