//@flow
import storeFactory from '~/store';
const {store} = storeFactory;
const {theme} = store.getState();

export function pickThemedColor(colors) {
  return colors[theme.currentTheme] || 'PRIMARY';
}

export function getTheme(): Object {
  return theme;
}
