export const sizing = {
  SMALLEST: 8,
  SMALLER: 11,
  SMALL: 14,
  MEDIUM: 16,
  LARGE: 18,
  LARGER: 22,
  LARGEST: 24,
};

export const styling = {
  LIGHT: 'Poppins Regular',
  REGULAR: 'Poppins Regular',
  MEDIUM: 'Poppins Medium',
  BOLD: 'Poppins Medium',
};
