import styled from 'styled-components/native';
import {spacing} from '~shared/styles/metrics';
import {sizing, styling} from '~shared/styles/fonts';
import {pickThemedColor} from '../helpers/Theme';
import Icons from 'react-native-feather1s';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${({theme}) => theme.BACKGROUND};
`;

export const CenteredRow = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Text = styled.Text`
  font-size: ${({sizeMultiplier, size}) =>
    (sizeMultiplier || 1) * sizing[size || 'MEDIUM']}px;

  font-family: ${({weight}) => 'arial' || styling[weight || 'REGULAR']};

  color: ${({theme, colors, lightColor, darkColor}) =>
    theme[
      colors ||
        pickThemedColor({light: lightColor, dark: darkColor}) ||
        'DARKEST'
    ]};
`;

export const Loading = styled.ActivityIndicator.attrs(
  ({size, color, theme}) => ({
    size: size || 'small',
    color: theme[color || 'PRIMARY'],
  }),
)``;

export const Icon = styled(Icons).attrs(
  ({theme, colors, lightColor, darkColor, size}) => ({
    color:
      theme[
        colors ||
          pickThemedColor({light: lightColor, dark: darkColor}) ||
          'DARKEST'
      ],

    size: spacing[size || 'MEDIUM'],
  }),
)``;
