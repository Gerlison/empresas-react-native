export const spacing = {
  SMALLEST: 4,
  SMALLER: 8,
  SMALL: 11,
  MEDIUM: 16,
  LARGE: 18,
  LARGER: 22,
  LARGEST: 28,
};

export const dimensions = {
  input: {
    HEIGHT: 45,
    PADDING: 11,
  },
  button: {
    HEIGHT: 50,
    MARGIN: spacing.LARGE,
    PADDING: spacing.MEDIUM,
  },
};
