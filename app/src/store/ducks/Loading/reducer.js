const INITIAL_STATE = {};

function capitalize(word) {
  const lowerWord = word.toLowerCase();
  return lowerWord.charAt(0).toUpperCase() + lowerWord.slice(1);
}

function getKey(verb, noun) {
  return `loading${capitalize(verb)}${capitalize(noun)}`;
}

export default function reducer(state = INITIAL_STATE, action) {
  const {type} = action;
  const matches = /([A-Z]*)_([A-Z]*)_(REQUEST|SUCCESS|FAILURE)/.exec(type);

  if (!matches) {
    return state;
  }

  const [, requestVerb, requestNoun, requestState] = matches;
  const requestName = getKey(requestVerb, requestNoun);

  return Object.assign({}, state, {
    [requestName]: requestState === 'REQUEST',
  });
}
