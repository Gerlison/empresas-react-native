import types from './types';
const {TOGGLE_THEME} = types;

function toggleTheme() {
  return {
    type: TOGGLE_THEME,
  };
}

export {toggleTheme};
