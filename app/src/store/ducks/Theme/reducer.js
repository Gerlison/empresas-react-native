import {LIGHT_THEME, DARK_THEME} from '~shared/styles/colors';

import types from './types';
const {TOGGLE_THEME} = types;

const INITIAL_STATE = {
  theme: LIGHT_THEME,
  currentTheme: 'light',
};

export default function(state = INITIAL_STATE, action) {
  const {type} = action;

  switch (type) {
    case TOGGLE_THEME:
      const {currentTheme} = state;
      return {
        ...state,
        currentTheme: currentTheme === 'light' ? 'dark' : 'light',
        theme: currentTheme === 'light' ? DARK_THEME : LIGHT_THEME,
      };

    default:
      return state;
  }
}
