const context = 'theme';

export default {
  TOGGLE_THEME: `${context}/TOGGLE_THEME`,
};
