import loading from './Loading/reducer';
import theme from './Theme/reducer';

export default {
  loading,
  theme,
};
