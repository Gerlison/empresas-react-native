import {createStore, combineReducers, applyMiddleware} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import createSagaMiddleware from 'redux-saga';

import rootSaga from './rootSaga';
import GlobalDucks from './ducks';
import Auth from '~/modules/Auth/ducks';
import Enterprise from '~/modules/Enterprise/ducks';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['theme'],
};

const sagaMiddleware = createSagaMiddleware();
const middlewares = applyMiddleware(sagaMiddleware);

const reducers = combineReducers(
  Object.assign({}, Auth, Enterprise, GlobalDucks),
);

const persistedReducer = persistReducer(persistConfig, reducers);

const storeFactory = () => {
  let store = createStore(persistedReducer, middlewares);
  let persistor = persistStore(store);
  return {store, persistor};
};

export default storeFactory();

sagaMiddleware.run(rootSaga);
