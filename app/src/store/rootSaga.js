import {all} from 'redux-saga/effects';
import authSagas from '~/modules/Auth/ducks/sagas';
import enterpriseSagas from '~/modules/Enterprise/ducks/sagas';

export default function* root() {
  yield all([authSagas(), enterpriseSagas()]);
}
